package br.com.moovon_ui.components

import android.content.Context
import android.graphics.Typeface
import androidx.annotation.IntDef
import androidx.core.content.res.ResourcesCompat
import br.com.moovon_ui.R


object TypeFaceManager {

    const val BOLD = 1
    const val REGULAR = 2

    @IntDef(
        BOLD,
        REGULAR
    )
    annotation class FontType

    fun get(context: Context, @FontType type: Int): Typeface? {

        val fontId = when (type) {
            BOLD -> R.font.montserrat_bold
            else -> R.font.montserrat
        }

        return ResourcesCompat.getFont(context, fontId)

    }

}