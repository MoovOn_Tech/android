package br.com.moovon_ui.components

import android.content.Context
import android.text.Html
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatTextView
import br.com.moovon_ui.R

class MoovTextView @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null
) : AppCompatTextView(context, attrs) {


    init {
        attrs?.let {
            val typedArray = context.obtainStyledAttributes(attrs,
                R.styleable.MoovTextView
            )
            val type = typedArray.getInt(
                R.styleable.MoovTextView_fontType,
                TypeFaceManager.REGULAR
            )
            setFontType(type)

            val html = typedArray.getString(R.styleable.MoovTextView_html)
            setHtml(html)

            typedArray.recycle()
        }
    }

    private fun setHtml(html: String?) {
        html?.let {
            val spanned = Html.fromHtml(html)
            text = spanned
        }
    }

    private fun setFontType(@TypeFaceManager.FontType type: Int) {
        typeface = TypeFaceManager.get(context, type)
    }

    fun setAvertaBold() {
        typeface = TypeFaceManager.get(
            context,
            TypeFaceManager.BOLD
        )
    }

    fun setAvertaRegular() {
        typeface = TypeFaceManager.get(
            context,
            TypeFaceManager.REGULAR
        )
    }

}