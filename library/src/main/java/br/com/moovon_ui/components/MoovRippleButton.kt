package br.com.moovon_ui.components

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.Button
import android.widget.FrameLayout
import androidx.annotation.IntDef
import androidx.core.content.ContextCompat
import br.com.moovon_ui.R
import br.com.moovon_ui.components.MoovRippleButton.ButtonManager.PRIMARY
import br.com.moovon_ui.components.MoovRippleButton.ButtonManager.SECONDARY

class MoovRippleButton @JvmOverloads constructor(
    context: Context,
    private val attrs: AttributeSet? = null,
    private val defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    private val btnMoovRipple by lazy { findViewById<Button>(R.id.btn_moov_ripple) }

    var text: String = ""
        set(value) {
            field = value
            btnMoovRipple.text = value
        }

    var buttonStyle: Int = PRIMARY
    set(value){
        field = value
        setButtonType(value)
    }

    init {
        LayoutInflater.from(context).inflate(R.layout.moov_ripple_button, this, true)

        attrs?.let {
            val typedArray = context.obtainStyledAttributes(
                attrs,
                R.styleable.MoovRippleButton,
                defStyleAttr,
                0
            )

            text = typedArray.getString(R.styleable.MoovRippleButton_android_text).orEmpty()
            buttonStyle = typedArray.getInt(R.styleable.MoovRippleButton_buttonType, PRIMARY)

            typedArray.recycle()
        }
    }

    private fun setButtonType(@ButtonManager.ButtonType buttonType: Int) {
        when (buttonType) {
            PRIMARY -> {
                btnMoovRipple.background =
                    ContextCompat.getDrawable(context, R.drawable.bg_ripple_primary)
                btnMoovRipple.setTextColor(
                    ContextCompat.getColor(
                        context,
                        R.color.colorWhite
                    )
                )
            }
            SECONDARY -> {
                btnMoovRipple.background =
                    ContextCompat.getDrawable(
                        context,
                        R.drawable.bg_ripple_secondary
                    )
                btnMoovRipple.setTextColor(
                    ContextCompat.getColor(
                        context,
                        R.color.colorPrimary
                    )
                )
            }
        }
    }

    private fun setText(txtButton: String) = txtButton

    override fun setOnClickListener(l: OnClickListener?) {
        super.setOnClickListener(l)
        btnMoovRipple.setOnClickListener(l)
    }

    object ButtonManager {
        const val PRIMARY = 1
        const val SECONDARY = 2

        @IntDef(PRIMARY, SECONDARY)
        annotation class ButtonType
    }
}