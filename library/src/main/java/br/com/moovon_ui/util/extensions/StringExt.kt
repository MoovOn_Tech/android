package br.com.moovon_ui.util.extensions

import java.text.SimpleDateFormat
import java.util.*

fun String?.getOnlyDigits(): String {
    val text = this ?: ""
    return text.replace("\\D".toRegex(), "")
}

fun String.parseCalendar(format: String = "dd/MM/yyyy"): Calendar {
    val sdf = SimpleDateFormat(format, Locale.getDefault())
    val calendar = Calendar.getInstance()
    calendar.time = sdf.parse(this) as Date
    return calendar
}

fun String?.unmask(): String {
    val text = this ?: ""
    return text.replace("[-/.()\\s]".toRegex(), "")
}

fun String.mask(mask: String): String {
    var masked = ""
    val unmasked = unmask().toCharArray().toMutableList()
    for (maskChar in mask.toCharArray()) {
        if (unmasked.isEmpty()) {
            break
        } else {
            masked += if (maskChar != '#') maskChar else unmasked.removeAt(0)
        }
    }

    return masked
}

fun String.maskReversed(mask: String): String {
    var masked = ""
    val unmasked = unmask().toCharArray().toMutableList()
    val maskReversed = mask.reversed()

    for (index in maskReversed.indices) {
        val maskChar = maskReversed[index]

        masked = if (unmasked.isNotEmpty()) {
            if (maskChar != '#') {
                maskChar
            } else {
                unmasked.removeAt(unmasked.size - 1)
            } + masked
        } else if (maskChar != '#' && maskReversed.substring(index).contains('#').not()) {
            maskChar + masked
        } else {
            break
        }
    }

    return masked

}