package br.com.moovon_ui.util.extensions

import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText

fun EditText.addMask(mask: String, isReversed: Boolean = false, afterChanged: (String) -> Unit = { }): TextWatcher {

    var currentText = text.toString()

    return doOnTextChanged { newText ->
        if (newText != currentText) {
            currentText = if (isReversed) newText.maskReversed(mask) else newText.mask(mask)
            setText(currentText)
            setSelection(currentText.length)
        }
        afterChanged(currentText)
    }
}

fun EditText.doOnTextChanged(
    onChanged: (newText: String) -> Unit
): TextWatcher = addTextChangedListener(onChangedString = onChanged)

fun EditText.addTextChangedListener(
    beforeChanged: ((s: CharSequence, start: Int, count: Int, after: Int) -> Unit)? = null,
    afterChaged: ((s: Editable) -> Unit)? = null,
    onChanged: ((s: CharSequence, start: Int, before: Int, count: Int) -> Unit)? = null,
    onChangedString: ((string: String) -> Unit)? = null
): TextWatcher {
    var currentText = text.toString()

    val watcher = object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            beforeChanged?.invoke(s, start, count, after)
        }

        override fun onTextChanged(newText: CharSequence, start: Int, before: Int, count: Int) {
            if (currentText != newText.toString()) {
                currentText = newText.toString()
                onChanged?.invoke(newText, start, before, count)
                onChangedString?.invoke(newText.toString())
            }
        }

        override fun afterTextChanged(s: Editable) {
            afterChaged?.invoke(s)
        }
    }

    addTextChangedListener(watcher)
    return watcher
}
