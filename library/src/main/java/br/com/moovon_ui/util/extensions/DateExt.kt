package br.com.moovon_ui.util.extensions

import java.util.*

fun Calendar.setAge(age: Int): Calendar {
    return Calendar.getInstance().apply {
        add(Calendar.YEAR, age)
        set(Calendar.HOUR, 11)
        set(Calendar.MINUTE, 59)
        set(Calendar.SECOND, 59)
        set(Calendar.AM_PM, Calendar.PM)
    }

}