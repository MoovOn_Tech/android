package br.com.moovon_ui.util

import br.com.moovon_ui.util.extensions.getOnlyDigits
import java.util.*
import java.util.regex.Pattern

fun String.isValidEmail(): Boolean {
    val email = this.toLowerCase()
    val pattern = Pattern.compile("\\b[a-z0-9._%+-]+@([a-z0-9-]+\\.)+[a-z]{2,12}\\b")

    return pattern.matcher(email).matches()
}

fun String.isValidToken() = this.length == 6

fun String.isValidName(): Boolean {
    val name = this.trim().toLowerCase()
    val pattern = Pattern.compile("(^[^<>'\"/;()+$`%#@!*&=_?.,{}\\[\\]:\\-\\d\\\\]*)")

    return pattern.matcher(name).matches()
}

fun String.isValidCompleteName(): Boolean {
    val name = this.trim().toLowerCase()
    val pattern = Pattern.compile("(^[^<>'\"/;()+$`%#@!*&=_?.,{}\\[\\]:\\-\\d\\\\]*)")
    val matcher = pattern.matcher(name)

    return matcher.matches() && name.isEmpty().not() && name.trim().split(" ").size > 1
}

fun String.isValidCellphoneNumber(): Boolean {

    val phone = this.getOnlyDigits()

    return if (phone.length == 11) {
        isValidDDD(this.substring(0, 2).toInt())
    } else {
        false
    }
}

private fun isValidDDD(ddd: Int): Boolean {
    return ddd >= 11
}

fun String.isValidDate(_isLenient: Boolean = false): Boolean {
    if (this.length < 10) return false

    val day = this.substring(0, 2).toInt()
    val month = this.substring(3, 5).toInt() - 1 //Month value is 0-based e.g for January
    val year = this.substring(0, 2).toInt()

    val cal = Calendar.getInstance().apply {
        isLenient = _isLenient
        set(year, month, day)
    }

    return try {
        cal.time
        true
    } catch (e: IllegalArgumentException) {
        false
    }

}

fun String.isValidCPF(): Boolean {
    var calc: Int
    var num = 10
    var sum = 0
    var rest = sum % 11

    for (x in 0..8) {
        calc = this[x].toString().toInt() * num
        sum += calc
        --num
    }

    var test = 11 - rest
    if (test > 9) test = 0
    if (test != this[9].toString().toInt()) return false
    num = 11
    sum = 0
    for (x in 0..9) {
        calc = this[x].toString().toInt() * num
        sum += calc
        --num
    }
    rest = sum % 11
    test = 11 - rest
    if (test > 9) test = 0
    if (test != this[10].toString().toInt()) return false
    return true
}
