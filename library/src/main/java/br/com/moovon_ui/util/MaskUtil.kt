package br.com.moovon_ui.util


abstract class MaskUtil {

    companion object {
        const val FORMAT_CPF = "###.###.###-##"
        const val FORMAT_PHONE_NUMBER = "(##) # ####-####"
        const val FORMAT_CEP = "#####-###"
        const val FORMAT_DATE = "##/##/####"
        const val FORMAT_TOKEN = "###-###"
        const val FORMAT_PASSWORD = "######"
    }

}