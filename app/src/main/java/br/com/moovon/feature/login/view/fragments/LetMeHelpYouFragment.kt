package br.com.moovon.feature.login.view.fragments

import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.navigation.fragment.findNavController
import br.com.moovon.R
import br.com.moovon.base.BaseFragment
import br.com.moovon.databinding.FragmentLetMeHelpYouBinding

class LetMeHelpYouFragment : BaseFragment() {

    override var fullscreen = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val callback = requireActivity().onBackPressedDispatcher.addCallback(this) {
            //do nothing
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_let_me_help_you, container, false)
    }

    private fun navigateToChatRegister() {
        findNavController().navigate(R.id.action_letMeHelpYouFragment_to_chatRegisterFragment)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        Handler().postDelayed({
            navigateToChatRegister()
        }, 3000)
    }

}