package br.com.moovon.feature.login.view.fragments

import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.animation.ObjectAnimator
import android.content.pm.PackageManager
import android.os.Bundle
import android.text.InputFilter
import android.text.InputType
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import br.com.moovon.R
import br.com.moovon.base.BaseFragment
import br.com.moovon.databinding.FragmentChatRegisterBinding
import br.com.moovon.feature.login.adapter.ButtonYesOrNoClickListener
import br.com.moovon.feature.login.adapter.RegisterChatAdapter
import br.com.moovon.feature.login.viewmodel.ChatRegisterViewModel
import br.com.moovon.feature.login.viewmodel.MoovChat
import br.com.moovon.feature.login.viewmodel.MoovChatFrom
import br.com.moovon.feature.login.viewmodel.MoovChatStep
import br.com.moovon_ui.util.MaskUtil
import br.com.moovon_ui.util.extensions.addMask
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ChatRegisterFragment : BaseFragment() {

    override var fullscreen = true

    private val binding by lazy { FragmentChatRegisterBinding.inflate(layoutInflater) }
    private val viewModel: ChatRegisterViewModel by viewModels()

    private var userAnswer = ""
    private var step = MoovChatStep.NONE

    private var textWatcher: TextWatcher? = null

    companion object {
        const val REQUEST_PERMISSION_LOCATION = 1234
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        introAnimation()
        setupViewModel()
        setupViews()
        setupChatAdapter()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = binding.root

    private fun introAnimation() {
        ObjectAnimator.ofFloat(
            binding.chatContainer,
            "translationY",
            2000f,
            0f
        ).apply {
            duration = 1000
            start()
        }
    }

    private fun setupViewModel() {
        lifecycle.addObserver(viewModel)
    }

    private fun setupViews() {
        val callback = requireActivity().onBackPressedDispatcher.addCallback(this) {
            findNavController().navigate(R.id.action_chatRegisterFragment_to_signInFragment)
        }

        binding.btnSend.setOnClickListener {
            userAnswer = binding.edtMessage.text.toString()
            viewModel.onClientAnswered(MoovChat(message = userAnswer, step = step, from = MoovChatFrom.USER))
            binding.edtMessage.text.clear()
            hideKeyboard()
        }
    }

    private fun onChoiceButtonClicked(btnText: String) {
        userAnswer = btnText
        viewModel.onClientAnswered(MoovChat(message = userAnswer, step = step, from = MoovChatFrom.USER))
    }

    private fun setupChatAdapter() {
        val buttonYesNoClickListener = object : ButtonYesOrNoClickListener {
            override fun onButtonYesOrNoClickListener(btnText: String) {
                when (step) {
                    MoovChatStep.CONFIRM_PASSWORD,
                    MoovChatStep.TOKEN_RECEIVED,
                    MoovChatStep.TOKEN_RECEIVED_RESEND,
                    MoovChatStep.LOCATION_TRY_AGAIN,
                    MoovChatStep.ADDRESS_STREET -> onChoiceButtonClicked(btnText)
                    else -> {
                        //doNothing
                    }
                }
            }
        }

        val adapter = RegisterChatAdapter(buttonYesNoClickListener)

        binding.rcvChat.apply {
            this.adapter = adapter
            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        }

        viewModel.fullChat.observe(this, Observer {
            it?.let { chatList ->
                step = chatList[chatList.lastIndex].step
                configEditText(step)
                adapter.submitList(chatList)
                binding.rcvChat.scrollToPosition(chatList.lastIndex)
            }
        })
    }

    private fun configEditText(step: MoovChatStep) {
        with(binding.edtMessage) {
            textWatcher?.let {
                removeTextChangedListener(it)
            }
            isEnabled = step != MoovChatStep.NONE
            when (step) {
                MoovChatStep.EMAIL -> inputType = InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS
                MoovChatStep.PASSWORD -> {
                    textWatcher = addMask(MaskUtil.FORMAT_PASSWORD)
                    inputType = InputType.TYPE_CLASS_NUMBER
                }
                MoovChatStep.TOKEN_RECEIVED -> isEnabled = false
                MoovChatStep.TOKEN_NUMBER -> {
                    textWatcher = addMask(MaskUtil.FORMAT_TOKEN)
                    inputType = InputType.TYPE_CLASS_NUMBER
                }
                MoovChatStep.SHORT_NAME -> inputType = InputType.TYPE_TEXT_VARIATION_PERSON_NAME
                MoovChatStep.PHONE_NUMBER -> {
                    textWatcher = addMask(MaskUtil.FORMAT_PHONE_NUMBER)
                    inputType = InputType.TYPE_CLASS_NUMBER
                }
                MoovChatStep.COMPLETE_NAME -> inputType = InputType.TYPE_TEXT_VARIATION_PERSON_NAME
                MoovChatStep.BIRTHDAY -> {
                    textWatcher = addMask(MaskUtil.FORMAT_DATE)
                    inputType = InputType.TYPE_CLASS_NUMBER
                }
                MoovChatStep.CPF -> {
                    textWatcher = addMask(MaskUtil.FORMAT_CPF)
                    inputType = InputType.TYPE_CLASS_NUMBER
                }
                MoovChatStep.LOCATION -> if (checkPermission().not()) requestLocationPermissions()
                MoovChatStep.ADDRESS_CEP -> {
                    textWatcher = addMask(MaskUtil.FORMAT_CEP)
                    inputType = InputType.TYPE_CLASS_NUMBER
                }
                MoovChatStep.ADDRESS_NUMBER -> inputType = InputType.TYPE_CLASS_NUMBER
                else -> {
                    inputType = InputType.TYPE_CLASS_TEXT
                }
            }
        }
    }

    private fun checkPermission(): Boolean {
        val result = ContextCompat.checkSelfPermission(requireContext(), ACCESS_FINE_LOCATION)

        return result == PackageManager.PERMISSION_GRANTED
    }

    private fun requestLocationPermissions() {
        requestPermissions(arrayOf(ACCESS_FINE_LOCATION), REQUEST_PERMISSION_LOCATION)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        when (requestCode) {
            REQUEST_PERMISSION_LOCATION -> {
                val isPermissionGrated =
                    grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED

                viewModel.onPermissionGranted(isPermissionGrated)

            }
        }
    }
}