package br.com.moovon.feature.login.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import br.com.moovon.R
import br.com.moovon.constants.ChatView
import br.com.moovon.feature.login.viewmodel.MoovChat
import br.com.moovon.feature.login.viewmodel.MoovChatFrom
import br.com.moovon.feature.login.viewmodel.MoovChatStep.*
import br.com.moovon_ui.components.MoovRippleButton
import br.com.moovon_ui.components.MoovTextView
import javax.inject.Inject

class RegisterChatAdapter @Inject constructor(
    private val buttonYesOrNoClickListener: ButtonYesOrNoClickListener,
) : ListAdapter<MoovChat, DefaultChatViewHolder>(ChatRegisterDiff()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DefaultChatViewHolder {
        val view = if (viewType == ChatView.DEFAULT.ordinal) {
            LayoutInflater.from(parent.context)
                .inflate(R.layout.view_register_chat_message_default, parent, false)
        } else {
            LayoutInflater.from(parent.context)
                .inflate(R.layout.view_register_chat_message_button, parent, false)
        }

        return ButtonChatViewHolder(view, buttonYesOrNoClickListener, parent.context)
    }

    override fun onBindViewHolder(holderDefault: DefaultChatViewHolder, position: Int) {
        holderDefault.bindView(getItem(position))
    }

    override fun getItemViewType(position: Int): Int {
        val isMoovonMessage = getItem(position).from == MoovChatFrom.MOOVON

        return when (getItem(position).step) {
            CONFIRM_PASSWORD,
            TOKEN_RECEIVED,
            TOKEN_RECEIVED_RESEND,
            LOCATION_TRY_AGAIN,
            ADDRESS_STREET -> if (isMoovonMessage) ChatView.CHOICE_BUTTON.ordinal else ChatView.DEFAULT.ordinal
            else -> ChatView.DEFAULT.ordinal
        }

    }
}

open class DefaultChatViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), ChatViewInterface {

    private val txtMessage: MoovTextView? = itemView.findViewById(R.id.txt_message)
    private val imgChat: ImageView? = itemView.findViewById(R.id.img_moovon_chat)

    override fun bindView(item: MoovChat) {
        txtMessage?.text = item.message

        val image = if (item.from == MoovChatFrom.MOOVON) R.drawable.ico_astronaut else R.drawable.ico_mario
        imgChat?.setImageDrawable(itemView.resources.getDrawable(image))
    }
}

class ButtonChatViewHolder @Inject constructor(
    itemView: View,
    private val buttonYesOrNoClickListener: ButtonYesOrNoClickListener,
    private val context: Context
) : DefaultChatViewHolder(itemView) {

    private val btnSecondary: MoovRippleButton? = itemView.findViewById(R.id.btn_secondary)
    private val btnPrimary: MoovRippleButton? = itemView.findViewById(R.id.btn_primary)

    override fun bindView(item: MoovChat) {
        super.bindView(item)

        when (item.step) {
            TOKEN_RECEIVED_RESEND -> {
                btnPrimary?.text = context.getString(R.string.home_register_chat_btn_resend_token)
                btnSecondary?.text = context.getString(R.string.home_register_chat_btn_change_email)
            }
            else -> {
                btnPrimary?.text = context.getString(R.string.home_register_chat_btn_no)
                btnSecondary?.text = context.getString(R.string.home_register_chat_btn_yes)
            }
        }

        btnPrimary?.setOnClickListener {
            buttonYesOrNoClickListener.onButtonYesOrNoClickListener(btnPrimary.text)
            hideButtons()
        }

        btnSecondary?.setOnClickListener {
            buttonYesOrNoClickListener.onButtonYesOrNoClickListener(btnSecondary.text)
            hideButtons()
        }
    }

    private fun hideButtons() {
        btnPrimary?.visibility = View.GONE
        btnSecondary?.visibility = View.GONE
    }
}

class ChatRegisterDiff : DiffUtil.ItemCallback<MoovChat>() {
    override fun areItemsTheSame(oldItem: MoovChat, newItem: MoovChat) = oldItem == newItem

    override fun areContentsTheSame(oldItem: MoovChat, newItem: MoovChat) = oldItem == newItem
}

interface ChatViewInterface {
    fun bindView(item: MoovChat)
}

interface ButtonYesOrNoClickListener {
    fun onButtonYesOrNoClickListener(button: String)
}