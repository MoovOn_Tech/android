package br.com.moovon.feature.login.view.host

import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupWithNavController
import br.com.moovon.R
import br.com.moovon.base.BaseActivity
import br.com.moovon.databinding.ActivityHomeLoginBinding
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

@AndroidEntryPoint
class HomeLoginActivity : BaseActivity() {

    private val binding by lazy { ActivityHomeLoginBinding.inflate(layoutInflater) }

    private lateinit var appBarConfiguration: AppBarConfiguration

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        setSupportActionBar(binding.ctnToolbar)

        val navController = this.findNavController(R.id.nav_host_fragment)
        appBarConfiguration = AppBarConfiguration(navController.graph)
        binding.ctnToolbar.setupWithNavController(navController, appBarConfiguration)

        navController.addOnDestinationChangedListener { controller, destination, _ ->
            when (destination.id) {
                R.id.signInFragment -> binding.ctnToolbar.visibility = View.GONE
                R.id.chatRegisterFragment -> {
                    binding.ctnToolbar.apply {
                        visibility = View.VISIBLE
                        navigationIcon = context.getDrawable(R.drawable.icon_close)
                        binding.titleToolbar.text = "Criar perfil"
                    }
                }
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = this.findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

}