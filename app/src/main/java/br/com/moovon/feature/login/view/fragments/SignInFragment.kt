package br.com.moovon.feature.login.view.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.addCallback
import androidx.navigation.fragment.findNavController
import br.com.moovon.R
import br.com.moovon.base.BaseFragment
import br.com.moovon.databinding.FragmentSignInBinding
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_home_login.*
import timber.log.Timber

@AndroidEntryPoint
class SignInFragment : BaseFragment() {


    override var fullscreen = true

    private val binding by lazy { FragmentSignInBinding.inflate(layoutInflater) }
    private val callbackManager: CallbackManager = CallbackManager.Factory.create()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        context.let {  }

        binding.btnFacebookLogin.setOnClickListener { requestFacebookLogin() }
        binding.btnEmail.setOnClickListener { navigateNext() }

        val callback = requireActivity().onBackPressedDispatcher.addCallback(this) {
            activity?.finish()
        }

        val activity = requireActivity()
        activity.ctn_toolbar.visibility = View.GONE

    }

    private fun navigateNext() {
        findNavController().navigate(R.id.action_signInFragment_to_letMeHelpYouFragment)
    }

    private fun requestFacebookLogin() {
        LoginManager.getInstance().logInWithReadPermissions(this, arrayListOf("email"))
        LoginManager.getInstance()
            .registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
                override fun onSuccess(result: LoginResult?) {
                    requestLoginInfos(result?.accessToken)
                }

                override fun onCancel() {
                    Toast.makeText(
                        requireContext(),
                        R.string.home_login_facebook_error,
                        Toast.LENGTH_SHORT
                    ).show()
                }

                override fun onError(error: FacebookException?) {
                    Timber.e(error?.printStackTrace().toString())
                    Toast.makeText(
                        requireContext(),
                        R.string.home_login_facebook_error,
                        Toast.LENGTH_SHORT
                    ).show()
                }
            })
    }

    private fun requestLoginInfos(accessToken: AccessToken?) {
        val profile = Profile.getCurrentProfile()
        val name = profile.name

        navigateNext()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return binding.root
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        callbackManager.onActivityResult(requestCode, resultCode, data)
        super.onActivityResult(requestCode, resultCode, data)
    }

}