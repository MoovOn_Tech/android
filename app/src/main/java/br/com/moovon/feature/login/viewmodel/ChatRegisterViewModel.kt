package br.com.moovon.feature.login.viewmodel

import android.content.Context
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.OnLifecycleEvent
import br.com.moovon.R
import br.com.moovon.base.BaseViewModel
import br.com.moovon.feature.login.ClientRegister
import br.com.moovon.feature.login.viewmodel.MoovChatStep.*
import br.com.moovon_ui.util.*
import br.com.moovon_ui.util.extensions.parseCalendar
import br.com.moovon_ui.util.extensions.setAge
import br.com.moovon_ui.util.extensions.unmask
import dagger.hilt.android.qualifiers.ApplicationContext
import java.util.*

class ChatRegisterViewModel @ViewModelInject constructor(
    @ApplicationContext val context: Context
) : BaseViewModel() {

    private val _fullChat = MutableLiveData<List<MoovChat>>()
    val fullChat: LiveData<List<MoovChat>>
        get() = _fullChat

    private var chat = mutableListOf<MoovChat>()

    private val clientRegister = ClientRegister(
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        ""
    )

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun onCreate() {
        nextMoovMessage(MoovChat(context.getString(R.string.home_register_chat_moovon_comment_1)))
        nextMoovMessage(MoovChat(context.getString(R.string.home_register_chat_moovon_whats_your_email), EMAIL))
    }

    fun onClientAnswered(answer: MoovChat) {
        chat.add(answer)
        _fullChat.postValue(chat)

        when (answer.step) {
            EMAIL -> checkEmail(answer.message)
            PASSWORD -> checkPassword(answer.message)
            CONFIRM_PASSWORD -> checkConfirmPassword(answer.message)
            TOKEN_RECEIVED -> checkSendToken(answer.message)
            TOKEN_RECEIVED_RESEND -> checkTokenResend(answer.message)
            TOKEN_NUMBER -> checkToken(answer.message)
            SHORT_NAME -> checkName(answer.message)
            PHONE_NUMBER -> checkPhoneNumber(answer.message)
            COMPLETE_NAME -> checkCompleteName(answer.message)
            BIRTHDAY -> checkBirthday(answer.message)
            CPF -> checkCPF(answer.message)
            LOCATION_TRY_AGAIN -> checkLocationPermission(answer.message)
            ADDRESS_CEP -> checkCep(answer.message)
            ADDRESS_STREET -> checkStreet(answer.message)
            ADDRESS_NUMBER -> checkAddressNumber(answer.message)
            ADDRESS_COMPLEMENT -> checkComplement(answer.message)
            COMPLETE_PROFILE -> checkCompleteProfile(answer.message)
            else -> {
                //doNothing
            }
        }
    }

    private fun checkEmail(email: String) {
        when {
            email.isValidEmail() -> {
                clientRegister.email = email

                nextMoovMessage(
                    MoovChat(context.getString(R.string.home_register_chat_moovon_password), PASSWORD)
                )
            }
            else -> {
                nextMoovMessage(
                    MoovChat(context.getString(R.string.home_register_chat_moovon_error_invalid_email), EMAIL)
                )
            }
        }
    }

    private fun checkPassword(password: String) {
        if (password.length == 6) {
            nextMoovMessage(
                MoovChat(context.getString(R.string.home_register_chat_moovon_confirm_password), CONFIRM_PASSWORD)
            )
        } else {
            nextMoovMessage(MoovChat(context.getString(R.string.home_register_chat_moovon_error_invalid_password_1)))
            nextMoovMessage(
                MoovChat(context.getString(R.string.home_register_chat_moovon_error_invalid_password_2), PASSWORD)
            )
        }
    }

    private fun checkConfirmPassword(isPasswordConfirmed: String) {
        if (isPasswordConfirmed == context.getString(R.string.home_register_chat_btn_yes)) {
            nextMoovMessage(MoovChat(context.getString(R.string.home_register_chat_moovon_confirm_password_yes)))
            nextMoovMessage(
                MoovChat(context.getString(R.string.home_register_chat_moovon_we_sent_you_a_code), TOKEN_RECEIVED)
            )
        } else {
            nextMoovMessage(MoovChat(context.getString(R.string.home_register_chat_moovon_confirm_password_no)))
            nextMoovMessage(
                MoovChat(context.getString(R.string.home_register_chat_moovon_error_invalid_password_2), PASSWORD)
            )
        }
    }

    private fun checkSendToken(isTokenReceived: String) {
        if (isTokenReceived == context.getString(R.string.home_register_chat_btn_yes)) {
            nextMoovMessage(
                MoovChat(
                    context.getString(R.string.home_register_chat_moovon_whats_your_token), TOKEN_NUMBER
                )
            )
        } else {
            nextMoovMessage(
                MoovChat(
                    context.getString(R.string.home_register_chat_moovon_error_resend_token), TOKEN_RECEIVED_RESEND
                )
            )
        }
    }

    private fun checkTokenResend(answer: String) {
        when (answer) {
            //Reenviar
            context.getString(R.string.home_register_chat_btn_resend_token) -> {
                nextMoovMessage(
                    MoovChat(context.getString(R.string.home_register_chat_moovon_we_sent_you_a_code), TOKEN_RECEIVED)
                )
            }

            //Email
            context.getString(R.string.home_register_chat_btn_change_email) -> {
                nextMoovMessage(MoovChat(context.getString(R.string.home_register_chat_moovon_whats_your_email), EMAIL))
            }
        }
    }

    private fun checkToken(token: String) {

        when {
            token.unmask().isValidToken().not() -> {
                nextMoovMessage(
                    MoovChat(
                        context.getString(R.string.home_register_chat_moovon_error_invalid_token), TOKEN_NUMBER
                    )
                )
            }
            else -> {
                clientRegister.token = token.unmask()
                nextMoovMessage(MoovChat(context.getString(R.string.home_register_chat_moovon_comment_2)))

                nextMoovMessage(MoovChat(context.getString(R.string.home_register_chat_moovon_comment_3)))

                nextMoovMessage(MoovChat(context.getString(R.string.home_register_chat_moovon_comment_4)))

                nextMoovMessage(
                    MoovChat(context.getString(R.string.home_register_chat_moovon_whats_your_name), SHORT_NAME)
                )

            }
        }
    }

    private fun checkName(shortName: String) {
        clientRegister.shortName = shortName

        nextMoovMessage(MoovChat(context.getString(R.string.home_register_chat_moovon_comment_5)))

        nextMoovMessage(
            MoovChat(context.getString(R.string.home_register_chat_moovon_whats_your_cellphone), PHONE_NUMBER)
        )
    }

    private fun checkPhoneNumber(phoneNumber: String) {
        when {
            phoneNumber.unmask().isValidCellphoneNumber().not() -> {
                nextMoovMessage(
                    MoovChat(
                        context.getString(R.string.home_register_chat_moovon_error_invalid_cellphone),
                        PHONE_NUMBER
                    )
                )
            }
            else -> {
                clientRegister.phoneNumber = phoneNumber.unmask()

                nextMoovMessage(
                    MoovChat(
                        context.getString(
                            R.string.home_register_chat_moovon_comment_6,
                            clientRegister.shortName
                        )
                    )
                )
                nextMoovMessage(MoovChat(context.getString(R.string.home_register_chat_moovon_comment_7)))

                nextMoovMessage(
                    MoovChat(
                        context.getString(R.string.home_register_chat_moovon_whats_your_full_name), COMPLETE_NAME
                    )
                )

            }
        }
    }

    private fun checkCompleteName(fullName: String) {
        if (fullName.isValidCompleteName()) {
            clientRegister.fullName = fullName
            nextMoovMessage(MoovChat(context.getString(R.string.home_register_chat_moovon_comment_8)))
            nextMoovMessage(
                MoovChat(context.getString(R.string.home_register_chat_moovon_whats_your_birthday), BIRTHDAY)
            )
        } else {
            nextMoovMessage(
                MoovChat(context.getString(R.string.home_register_chat_moovon_error_invalid_name), COMPLETE_NAME)
            )
        }
    }

    private fun checkBirthday(birthday: String) {
        val age = Calendar.getInstance()

        when {
            birthday.isValidDate().not() -> {
                nextMoovMessage(
                    MoovChat(context.getString(R.string.home_register_chat_moovon_whats_your_birthday), BIRTHDAY)
                )
            }
            age.setAge(-16).before(birthday.parseCalendar()) -> {
                nextMoovMessage(MoovChat(context.getString(R.string.home_register_chat_moovon_error_lower_16_1)))
                nextMoovMessage(MoovChat(context.getString(R.string.home_register_chat_moovon_error_lower_16_2)))
                nextMoovMessage(MoovChat(context.getString(R.string.home_register_chat_moovon_error_lower_16_3)))
                nextMoovMessage(MoovChat(context.getString(R.string.home_register_chat_moovon_error_lower_16_4)))
            }

            age.setAge(-17).after(birthday.parseCalendar())
                    && age.setAge(-20).before(birthday.parseCalendar()) -> {

                nextMoovMessage(MoovChat(context.getString(R.string.home_register_chat_moovon_error_age_between_16_19)))
                nextMoovMessage(MoovChat(context.getString(R.string.home_register_chat_moovon_comment_9), CPF))
            }

            age.setAge(-21).after(birthday.parseCalendar())
                    && age.setAge(-25).before(birthday.parseCalendar()) -> {

                clientRegister.birthday = birthday
                nextMoovMessage(MoovChat(context.getString(R.string.home_register_chat_moovon_error_age_between_20_25)))
                nextMoovMessage(MoovChat(context.getString(R.string.home_register_chat_moovon_comment_9), CPF))
            }

            age.setAge(-26).after(birthday.parseCalendar())
                    && age.setAge(-30).before(birthday.parseCalendar()) -> {

                clientRegister.birthday = birthday
                nextMoovMessage(MoovChat(context.getString(R.string.home_register_chat_moovon_error_age_between_26_30)))
                nextMoovMessage(MoovChat(context.getString(R.string.home_register_chat_moovon_comment_9), CPF))
            }
            age.setAge(-31).after(birthday.parseCalendar())
                    && age.setAge(-40).before(birthday.parseCalendar()) -> {

                clientRegister.birthday = birthday
                nextMoovMessage(MoovChat(context.getString(R.string.home_register_chat_moovon_error_age_between_31_40)))
                nextMoovMessage(MoovChat(context.getString(R.string.home_register_chat_moovon_comment_9), CPF))
            }
            age.setAge(-40).after(birthday.parseCalendar()) -> {
                clientRegister.birthday = birthday
                nextMoovMessage(MoovChat(context.getString(R.string.home_register_chat_moovon_error_greater_40)))
                nextMoovMessage(MoovChat(context.getString(R.string.home_register_chat_moovon_comment_9), CPF))
            }
        }

        clientRegister.birthday = birthday
    }

    private fun checkCPF(cpfNumber: String) {
        clientRegister.cpf = cpfNumber
        nextMoovMessage(MoovChat(context.getString(R.string.home_register_chat_moovon_comment_10)))
        nextMoovMessage(MoovChat(context.getString(R.string.home_register_chat_moovon_comment_11)))
        nextMoovMessage(MoovChat(context.getString(R.string.home_register_chat_moovon_require_location), LOCATION))
    }

    fun onPermissionGranted(isLocationPermissionGranted: Boolean) {
        if (isLocationPermissionGranted) {
            nextMoovMessage(MoovChat(context.getString(R.string.home_register_chat_moovon_comment_20)))
            nextMoovMessage(MoovChat(context.getString(R.string.home_register_chat_moovon_comment_cep), ADDRESS_CEP))
        } else {
            nextMoovMessage(MoovChat(context.getString(R.string.home_register_chat_moovon_error_location_permission_1)))
            nextMoovMessage(
                MoovChat(
                    context.getString(R.string.home_register_chat_moovon_error_location_permission_2),
                    LOCATION_TRY_AGAIN
                )
            )
        }
    }

    private fun checkLocationPermission(tryPermissionAgain: String) {
        if (tryPermissionAgain == context.getString(R.string.home_register_chat_btn_yes)) {
            nextMoovMessage(MoovChat(context.getString(R.string.home_register_chat_moovon_require_location), LOCATION))
        } else {
            nextMoovMessage(MoovChat(context.getString(R.string.home_register_chat_moovon_error_location_permission_3)))
            nextMoovMessage(MoovChat(context.getString(R.string.home_register_chat_moovon_error_location_permission_4)))
        }
    }

    private fun checkCep(cep: String) {
        //TODO: Get Cep
        clientRegister.cep = cep.unmask()
        nextMoovMessage(MoovChat(context.getString(R.string.home_register_chat_moovon_comment_street), ADDRESS_STREET))
    }

    private fun checkStreet(isStreet: String) {
        if (isStreet == context.getString(R.string.home_register_chat_btn_yes)) {
            nextMoovMessage(
                MoovChat(context.getString(R.string.home_register_chat_moovon_comment_addrress_number), ADDRESS_NUMBER)
            )
        } else {
            nextMoovMessage(MoovChat(context.getString(R.string.home_register_chat_moovon_comment_cep), ADDRESS_CEP))
        }
    }

    private fun checkAddressNumber(addressNumber: String) {
        clientRegister.addressNumber = addressNumber
        nextMoovMessage(
            MoovChat(
                context.getString(R.string.home_register_chat_moovon_comment_addrress_complement), ADDRESS_COMPLEMENT
            )
        )
    }


    private fun checkComplement(complement: String) {
        clientRegister.addressComplement = complement

        nextMoovMessage(
            MoovChat(
                context.getString(
                    R.string.home_register_chat_moovon_comment_21,
                    clientRegister.shortName
                )
            )
        )
        nextMoovMessage(MoovChat(context.getString(R.string.home_register_chat_moovon_comment_22), COMPLETE_PROFILE))
    }

    private fun checkCompleteProfile(completeProfile: String) {

        if (completeProfile == context.getString(R.string.home_register_chat_btn_yes)) {
            nextMoovMessage(
                MoovChat(
                    context.getString(
                        R.string.home_register_chat_moovon_comment_complete_yes,
                        clientRegister.shortName
                    )
                )
            )
            //TODO: Colocar na tela do perfil
        } else {
            nextMoovMessage(
                MoovChat(
                    context.getString(
                        R.string.home_register_chat_moovon_comment_complete_no,
                        clientRegister.shortName
                    )
                )
            )
            //TODO: Colocar na tela do feed
        }

    }

    private fun nextMoovMessage(moovChat: MoovChat) {
        chat.add(moovChat)
        _fullChat.postValue(chat)
    }
}

data class MoovChat(
    val message: String = "",
    val step: MoovChatStep = NONE,
    val from: MoovChatFrom = MoovChatFrom.MOOVON
)

enum class MoovChatStep {
    EMAIL,
    PASSWORD,
    CONFIRM_PASSWORD,
    TOKEN_RECEIVED,
    TOKEN_RECEIVED_RESEND,
    TOKEN_NUMBER,
    SHORT_NAME,
    PHONE_NUMBER,
    COMPLETE_NAME,
    BIRTHDAY,
    CPF,
    LOCATION,
    LOCATION_TRY_AGAIN,
    ADDRESS_CEP,
    ADDRESS_STREET,
    ADDRESS_NUMBER,
    ADDRESS_COMPLEMENT,
    COMPLETE_PROFILE,
    NONE
}

enum class MoovChatFrom {
    USER,
    MOOVON
}

