package br.com.moovon.feature.login

data class ClientRegister(
    var email: String,
    var token: String,
    var shortName: String,
    var phoneNumber: String,
    var fullName: String,
    var birthday: String,
    var cpf: String,
    var cep: String,
    var addressStreet: String,
    var addressNumber: String,
    var addressComplement: String
)
