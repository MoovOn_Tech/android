package br.com.moovon.feature.login.view.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.moovon.R
import br.com.moovon.base.BaseFragment

class NowOpportunitiesFragment : BaseFragment() {

    override var fullscreen = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_let_me_help_you, container, false)
    }

}