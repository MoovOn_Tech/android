package br.com.moovon.base

import android.view.View
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.ViewModel

open class BaseViewModel : ViewModel(), LifecycleObserver {

    override fun onCleared() {
        super.onCleared()
    }

}