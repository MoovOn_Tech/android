package br.com.moovon.base

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

abstract class BaseActivity : AppCompatActivity() {

    @Inject
    @ApplicationContext
    lateinit var context: Context

}