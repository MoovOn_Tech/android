package br.com.moovon.constants

enum class ChatView {
    DEFAULT,
    CHOICE_BUTTON,
    LIST
}